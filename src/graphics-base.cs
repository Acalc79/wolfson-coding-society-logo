using System;

public class Point
{
    public readonly double x;
    public readonly double y;
    public Point(double x, double y)
    {
        this.x = x;
        this.y = y;
    }

    public override string ToString()
    {
        return String.Format("({0:E2},{1:E2})", x, y);
    }
}

public class Rect
{
    public readonly Point Center;
    public readonly double Height;
    public readonly double Width;
    public Rect(Point center, double h, double w)
    {
        Center = center;
        Height = h;
        Width = w;
    }

    public bool IntersectsCircle(Point center, double radius)
    {
        var dx = Math.Abs(center.x - Center.x) - Width/2;
        var dy = Math.Abs(center.y - Center.y) - Height/2;
        if (dx < 0)
            return dy < radius;
        if (dy < 0)
            return dx < radius;
        return dx*dx + dy*dy < radius*radius;
    }
}

public class Viewport : Rect
{
    public readonly int ImageWidth;
    public readonly int ImageHeight;
    public readonly double PixelSize;

    public Viewport(Point center, double h,
                    int imageWidth, int imageHeight)
    : base(center, h, h * imageWidth / imageHeight)
    {
        ImageWidth = imageWidth;
        ImageHeight = imageHeight;
        PixelSize = h / imageHeight;
    }
}
