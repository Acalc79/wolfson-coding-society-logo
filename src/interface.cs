using System.Collections.Generic;

namespace SVG
{
    public class Attribute
    {
        public readonly string Content;
        public readonly string Name;
        
        public string XML()
        => String.Format("{0}=\"{1}\"", Name, Content);
        
        public Attribute(string encoded)
        {
            // TODO: decode attribute string
        }
        public Attribute(string name, string content)
        {
            AttributeName = name;
            Content = content;
        }
    }
    
    public class ViewBox : Attribute
    {
        public readonly Rect Size;
        
        public ViewBox(Rect size)
        : base("viewBox", String.Format("{0} {1} {2} {3}",
                                        size.Center.x - size.Width/2,
                                        size.Center.y - size.Height/2,
                                        size.Width, size.Height))
        {
            Size = size;
        }
    }
    
    public class Element
    {
        public readonly string Kind;
        public readonly List<Attribute> Attributes;
        private List<Element> Elements;
        
        public readonly string Open, Close;

        public Element(string String)
        {
            // TODO: decode element from string
        }
        public Element(string kind,
                       IEnumerable<Attribute> attributes,
                       IEnumerable<Element> elements)
        {
            Kind = kind;
            Attributes = attributes.ToList();
            Elements = elements.ToList();
            Open = String.Format(
                "<{0} {1}>",
                Kind,
                String.Join(" ", Attributes.Select(Attribute.XML)));
            Close = String.Format("</{0}>", Kind);
        }

        public string EmptyXML() => Open.Trim('>') + "/>";
        public virtual string XML()
        {
            if (! Attributes.Any())
                return EmptyXML();
            string content = String.Join(
                "\n",
                Elements.SelectMany(
                    e => e.XMLAttribute
                    .Split(new[] { "\r\n", "\r", "\n" },
                           StringSplitOptions.RemoveEmptyEntries)
                    .Select(line => "  " + line)));
            return String.Format("{0}\n{1}\n{2}", Open, content, Close);

        }
    }
    
    public class Surface : Element
    {
        const string HEADER = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
        public readonly ViewBox Box;
        public Surface(ViewBox box, IEnumerable<Element> elems)
        : base("svg",
               new Attribute[]{new Attribute("version", "1.1"), Box},
               elems)
        {
            Box = box;
        }

        public override string XML() => HEADER + base.XML();
    }
}
