using System;
using System.Linq;
using System.Threading; // for Thread.CurrentThread.CurrentCulture
using System.Globalization; // for CultureInfo

public class Logo
{
    // nubmer of sub-logos, here size of the English alphabet
    const int N = 26;
    // derived from: 2*pi*R = 2*(R*scale)*N
    const double SCALE_FACTOR = 9;
    // derived from: 1 = unit + 2/scale
    const double UNIT_SIZE = 7/9.0;

    private readonly LogoUnit Unit;
    private readonly Point Center;
    private readonly double Radius;
    private readonly Lazy<Logo>[] Sublogos;


    public Logo(string text, Point center, double radius)
    {
        Unit = new LogoUnit(text);
        Center = center;
        Radius = radius;
        Sublogos = Enumerable.Range(0, N)
            .Select(i => new Lazy<Logo>(() => MakeSublogo(i)))
            .ToArray();
    }

    private Logo MakeSublogo(int index)
    {
        char letter = (char) ((int) 'a' + index);
        // measured into the screen (clockwise) with 0 at the top
        double angle = 2 * Math.PI * index / N;
        double dist = Radius * (UNIT_SIZE + 1/SCALE_FACTOR);
        double dx = dist * Math.Sin(angle);
        double dy = dist * Math.Cos(angle);
        return new Logo(letter.ToString(),
                        new Point(Center.x + dx, Center.y + dy),
                        Radius / SCALE_FACTOR);
    }
    
    public void Draw(Viewbox viewbox)
    {
        if (! viewbox.IntersectsCircle(Center, Radius))
        {
            return; // not in view
        }
        if (Radius < viewbox.PixelSize)
        {
            return; // too small to be visible
        }
        if (viewbox.IntersectsCircle(Center, Radius * UNIT_SIZE))
        {
            Console.WriteLine(String.Format("Drawing unit at {0}", Center));
            // unit.DrawAt(position, scale)
        }
        foreach (var logo in Sublogos.Select(lazy => lazy.Value))
        {
            logo.Draw(viewbox);
        }
    }

    public static void Main(string[] args)
    {
        Thread.CurrentThread.CurrentCulture = CultureInfo.GetCultureInfo("en-GB");
        // main logo is centered at (0,0)
        // and contained within a circle of radius 1
        var center = new Point(0,0);
        var logo = new Logo("", center, 1);
        var viewbox = new Viewbox(center, 2, 720, 405);
        Console.WriteLine(
            String.Format("Pixel size is {0}", viewbox.PixelSize));
        logo.Draw(viewbox);
    }
}

public class LogoUnit : IDrawable
{
    public const string SVG_PATH = "";

    public readonly string text;
    public static readonly SVG image;

    static LogoUnit()
    {
        image = new SVG(SVG_PATH);
    }

    public LogoUnit(string text)
    {
        this.text = text;
    }

    public void DrawAt(Point position, double scale)
    {
        // draw the logo unit
    }
}
